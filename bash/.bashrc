#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias et='emacsclient -t --alternate-editor=""'
alias ec='emacsclient -c --alternate-editor=""'

PS1='[\u@\h \W]\$ '

[[ -f ~/.bashrc.local ]] && . ~/.bashrc.local
