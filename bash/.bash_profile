#!/bin/bash
#
# This file is sourced by bash for login shells (`bash -l`, `-/bin/bash`).
#   Login shells: linux console (tty), ssh.
#   Non-login shells: terminal emulators (typically).
#
# The following line sources .bashrc and is recommended by http://mywiki.wooledge.org/DotFiles
#

export TERMINAL=xterm # For i3-sensible-terminal, else uxrvt gets preference

[[ -f ~/.bashrc ]] && . ~/.bashrc
